import {  Resolver, Query, Args} from '@nestjs/graphql';
@Resolver('User')
export class UserResolver {
  constructor(){

  }

  @Query('user')
  getUser(@Args('id') id: string){
      return {
          id: 'id',
          userName: 'userName',
          password: 'password',
          name: 'name',
          role: 'NORMAL',
      };
  }
  @Query('users')
  getAllUsers() {
      return [
          {
            id: 'id',
            userName: 'userName',
            password: 'password',
            name: 'name',
            role: 'ADMIN',
          },
      ];
  }
}