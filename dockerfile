FROM node:8.2.1-slim

EXPOSE 3000 

ENV HOME=/home/src
# Install app dependencies
COPY package.json yarn.lock $HOME/app/

# Create app directory
WORKDIR $HOME/app

RUN yarn
