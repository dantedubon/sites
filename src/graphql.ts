export enum Role {
    ADMIN = "ADMIN",
    NORMAL = "NORMAL"
}

export abstract class IQuery {
    abstract user(id: string): User | Promise<User>;

    abstract users(): User[] | Promise<User[]>;

    abstract temp__(): boolean | Promise<boolean>;
}

export class User {
    id: string;
    userName: string;
    password: string;
    name?: string;
    role: Role;
}
